import sys

from PySide6 import QtWidgets

from src.AppMainWindow import AppMainWindow

if __name__ == "__main__":
	app = QtWidgets.QApplication(sys.argv)
	
	# Stylesheet
	try:
		with open("res/style.qss", "r") as file:
			app.setStyleSheet(file.read())
	except OSError as e:
		print(f"Reading stylesheet error: {type(e)}:{e}")
        
	main_window = AppMainWindow()
	sys.exit(app.exec())
