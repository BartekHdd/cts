class CsvParser:
	def open(self, file_path):
		columns = 0 #Max columns
		data = []
		try:
			with open(file_path, "r") as file:
				if (lines := file.readlines()) == []:
					return "Selected file is empty"
				for line in lines:
					splitted = line.replace('\n', '').split(",")
					data.append(splitted)
					if (number_of := len(splitted)) > columns:
						columns = number_of
		except OSError as e:
			return f"File reading error: {e}"

		return {"rows": len(data), "columns": columns, "data": data}
