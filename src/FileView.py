from PySide6 import QtWidgets, QtGui, QtCore, Qt

from src.CsvParser import CsvParser
from src.TableWidget import TableWidget

class FileView(QtWidgets.QWidget):
	def __init__(self):
		super().__init__()
		
		# Table
		self.table = TableWidget()
		self.table.hide()
		
		# Selecting button
		self.select_file_button = QtWidgets.QPushButton("Select File")
		self.select_file_button.setMaximumWidth(100)
		self.select_file_button.clicked.connect(self.choose_file)
		
		# Center the selecting button
		button_layout = QtWidgets.QHBoxLayout()
		button_layout.addStretch()
		button_layout.addWidget(self.select_file_button)
		button_layout.setAlignment(QtCore.Qt.AlignVCenter)
		button_layout.addStretch()
		
		# File opening message
		self.opening_file_label = QtWidgets.QLabel()
		self.opening_file_label.setText("Opening File...")
		self.opening_file_label.setAlignment(QtCore.Qt.AlignCenter | QtCore.Qt.AlignCenter)
		self.opening_file_label.hide()
		
		# File error message
		self.file_error_label = QtWidgets.QLabel()
		self.file_error_label.setText("")
		self.file_error_label.setAlignment(QtCore.Qt.AlignCenter)
		self.file_error_label.setMaximumHeight(100);
		self.file_error_label.hide()
		self.file_error_label.setObjectName("error_message")
		
		# Container for the selecting button, table and error messages
		self.layout = QtWidgets.QVBoxLayout(self)
		self.layout.setAlignment(QtCore.Qt.AlignCenter)
		self.layout.addLayout(button_layout)
		self.layout.addWidget(self.opening_file_label)
		self.layout.addWidget(self.file_error_label)
		self.layout.addWidget(self.table)
		
		# File selection dialog
		self.select_file_dialog = FileOpenDialog()
		
		self.file_parser = CsvParser()
	
	def choose_file(self):
		# Visible opening message only
		self.select_file_button.hide()
		self.file_error_label.hide()
		self.opening_file_label.show()
		
			
		if self.select_file_dialog.exec():
			# Parser returns string for an error instead of dictionary
			if isinstance(output := self.file_parser.open(self.select_file_dialog.selectedFiles()[0]), str):
				# Visible error message and selecting button only
				self.file_error_label.setText(output)
				self.file_error_label.show()
				self.select_file_button.show()
				self.opening_file_label.hide()	
			else:
				# Visible container only
				self.table.load(output)
				self.opening_file_label.hide()
				self.table.show()
		else:
			# Visible select file button only
			self.select_file_button.show()
			self.file_error_label.hide()
			self.opening_file_label.hide()

class FileOpenDialog(QtWidgets.QFileDialog):
	def __init__(self):
		super().__init__()
		
		self.setFileMode(QtWidgets.QFileDialog.ExistingFile)
		self.setWindowTitle("Select Csv File, Please")
		self.setNameFilter("CSV (*.csv)")
