from PySide6 import QtWidgets
from PySide6.QtWidgets import QWidget, QTableWidget, QVBoxLayout

class TableWidget(QWidget):
	def __init__(self):
		super().__init__()

		self.table = QTableWidget(self)
		layout = QVBoxLayout(self)
		layout.addWidget(self.table)
		self.setLayout(layout)
				
		self.table.verticalHeader().setVisible(False)
		self.table.horizontalHeader().setVisible(False)
		self.table.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
		
	def load(self, data):
		self.table.setRowCount(data["rows"])
		self.table.setColumnCount(data["columns"])
		for row, columns in enumerate(data["data"]):
			for column, value in enumerate(columns):
				self.table.setItem(row, column, QtWidgets.QTableWidgetItem(str(value)))
		self.table.resizeColumnsToContents()
