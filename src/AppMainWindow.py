from PySide6.QtWidgets import QMainWindow

from src.FileView import FileView

class AppMainWindow(QMainWindow):
	def __init__(self):
		super().__init__()
		
		self.file_view = FileView()
		self.setCentralWidget(self.file_view)
		
		self.setGeometry(300, 300, 800, 600)
		self.setWindowTitle("Csv Table Showing Application")
		self.show()
